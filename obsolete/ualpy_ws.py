# -*- coding: utf-8 -*-
from werkzeug.wrappers import Response
from werkzeug.wrappers import Request as RequestBase
from werkzeug.contrib.wrappers import JSONRequestMixin
from werkzeug.routing import Map, Rule
from werkzeug.exceptions import HTTPException, NotFound, MethodNotAllowed, BadRequest
# from werkzeug.wsgi import SharedDataMiddleware
from werkzeug.utils import redirect
import json
import matplotlib as mpl
import matplotlib.pylab as pl
import numpy as np
from multiprocessing import Process
import sys
import StringIO
import contextlib
import traceback
from itertools import chain
import copy


context = {}
context['init'] = False
context['data'] = {}
context['ual'] = {}
context['figures'] = {}
context['exec_context'] = {'__builtins__': globals()['__builtins__'],
                           '__name__': '__main__',
                           '__doc__': None,
                           '__package__': None}

url_map = Map([
    Rule('/', endpoint='root'),
    Rule('/init', endpoint='init'),
    Rule('/init/<int(min=1,max=999999):shot>/'
         '<int(min=1,max=9999):run>/'
         '<int(min=0):idx>', endpoint='init'),
    Rule('/finish', endpoint='finish'),
    Rule('/context', endpoint='context'),
    Rule('/put', endpoint='put'),
    Rule('/put_cpo', endpoint='put_cpo'),
    Rule('/plot', endpoint='plot'),
    Rule('/plot/<int(min=0):fignum>', endpoint='plot'),
    Rule('/exec', endpoint='exec'),
    # Rule('/new_session', endpoint='new_session'),
    # Rule('/close_session/<session_id>', endpoint='close_session'),
    # Rule('/<session_id>', endpoint='session_info'),
])

NO_INIT = ('root', 'init', 'finish')


@contextlib.contextmanager
def stdoutIO(stdout=None):
    old = sys.stdout
    if stdout is None:
        stdout = StringIO.StringIO()
    sys.stdout = stdout
    yield stdout
    sys.stdout = old


class Request(RequestBase, JSONRequestMixin):
    pass


def dispatch_request(request):
    adapter = url_map.bind_to_environ(request.environ)
    try:
        endpoint, values = adapter.match()
        func = globals()['on_' + endpoint]
        if endpoint not in NO_INIT:
            func = check_init(func)
        return func(request, **values)
    except HTTPException, e:
        return e


def check_init(func):
    def new_func(*args, **kwargs):
        if not context['init']:
            text = "You must use init first"
            res = Response(text, mimetype='text/plain')
        else:
            res = func(*args, **kwargs)
        return res
    return new_func


def on_root(request, **values):
    text = """UALPython-WS

Usage:
...
"""
    response = Response(text, mimetype='text/plain')
    return response


def on_init(request, **values):
    if 'shot' in values and 'run' in values and 'idx' in values:
        # UAL init
        import ual
        context['ual']['ual'] = ual
        context['ual']['shot'] = int(values['shot'])
        context['ual']['run'] = int(values['run'])
        context['ual']['idx'] = int(values['idx'])
        context['ual']['input_cpo_names'] = []
        context['ual']['itm_obj'] = ual.itm(context['ual']['shot'],
                                            context['ual']['run'], 0, 0)
        context['ual']['itm_obj'].open()
        text = 'Successfully initialized UAL'
    else:
        text = 'Successfully initialized (no UAL)'
        context['ual'] = {}
    response = Response(text, mimetype='text/plain')
    context['init'] = True
    return response


def on_finish(request, **values):
    if context['ual']:
        try:
            context['ual']['itm_obj'].close()
        except Exception:
            pass
        context['ual'] = {}
    context['init'] = False
    context['data'] = {}
    for p in context['figures'].values():
        p.terminate()
    context['figures'] = {}
    text = 'Finished'
    response = Response(text, mimetype='text/plain')
    return response


def on_context(request, **values):
    text = json.dumps(stringify(context, fstr=repr), indent=4)
    response = Response(text, mimetype='application/json')
    return response


def stringify(data, fstr=repr):
    if hasattr(data, 'items'):
        res = {}
        for k, v in data.items():
            res[k] = stringify(v, fstr=fstr)
    elif isinstance(data, list):
        res = [stringify(x, fstr=fstr) for x in data]
    elif isinstance(data, (str, unicode, int, long, float)):
        res = data
    else:
        res = fstr(data)
    return res


def on_put(request, **values):
    text = json.dumps({})
    if request.method == 'PUT':
        try:
            data = request.json
            context['data'].update(data)
            text = json.dumps(context['data'])
        except Exception as e:
            response = Response(str(e), mimetype='text/plain')
            return response
    else:
        e = MethodNotAllowed(valid_methods=['PUT'],
                             description='PUT method expected')
        return e.get_response()
    response = Response(text, mimetype='application/json')
    return response


def on_put_cpo(request, **values):
    text = json.dumps({})
    if not context['ual']:
        response = Response('UAL is not initialized', mimetype='text/plain')
        return response
    if request.method == 'PUT':
        # cpo, idx, machine, occurrence, run, shot, user, version
        try:
            cpo_dict = request.json
            # read the cpo data
            cpo_name = cpo_dict['cpo'] + str(cpo_dict['occurrence'])
            time = context['data'].get('time')
            interpolation = context['data'].get('interpolation', 1)
            context['ual'][cpo_name] = get_cpo(cpo_dict, context['ual']['itm_obj'],
                                               time, interpolation)
            if cpo_name not in context['ual']['input_cpo_names']:
                context['ual']['input_cpo_names'].append(cpo_name)
            text = '%s data successfully read' % cpo_name
        except Exception as e:
            s = StringIO.StringIO()
            traceback.print_exc(file=s)
            text = s.getvalue()
            response = Response(text, mimetype='text/plain')
            return response
    else:
        e = MethodNotAllowed(valid_methods=['PUT'],
                             description='PUT method expected')
        return e.get_response()
    response = Response(text, mimetype='application/json')
    return response


def get_cpo(cpo_dict, itm_obj, time=None, interpolation=1):
    # TODO cross check shot/run
    if time is None and cpo_dict.get('time') is not None:
        time = cpo_dict['time']
    if time is None:
        attr = '%sArray' % cpo_dict['cpo']
        cpo = getattr(itm_obj, attr)
        cpo.get(int(cpo_dict['occurrence']))
    else:
        attr = cpo_dict['cpo']
        cpo = getattr(itm_obj, attr)
        cpo.getSlice(time, interpolation, int(cpo_dict['occurrence']))
    # TODO why deepcopy ???
    cpo = copy.deepcopy(cpo)

    return cpo


def on_plot(request, **values):
    if 'fignum' in values:
        fignum = int(values['fignum'])
    elif context['figures']:
        fignum = max(context['figures'].keys()) + 1
    else:
        fignum = 1
    if fignum in context['figures']:
        # close the old figure
        # TODO: update instead
        context['figures'][fignum].terminate()
    p = Process(target=plot, args=(fignum, ))
    context['figures'][fignum] = p
    p.start()
    # p.join()
    response = Response('Hello Plot %i!' % fignum, mimetype='text/plain')
    return response


def on_exec(request, **values):
    if request.method == 'PUT':
        try:
            code = request.json['code']
        except Exception:
            text = ('JSON request not in a proper format\n\n'
                    '{"code": code_source}\n'
                    'optionally {"code": code_source,\n'
                    '"output": [output_variables]}')
            s = StringIO.StringIO()
            traceback.print_exc(file=s)
            text += '\n\n' + s.getvalue()
            e = BadRequest(description=text)
            return(e.get_response())
        exec_context = context['exec_context']
        exec_context.update(context['ual'])
        exec_context.update(context['data'])

        # debug
        # j = {'code': code, 'context': str(exec_context.keys())}
        # response = Response(json.dumps(j), mimetype='application/json')

        with stdoutIO() as s:
            try:
                exec(code, exec_context)
            except Exception:
                traceback.print_exc(file=s)
            finally:
                response = Response(s.getvalue(), mimetype='text/plain')

        # update data with new variables
        for key in exec_context.keys():
            if key in chain(context['data'],
                            request.json.get('output', ())):
                context['data'][key] = exec_context[key]
            # TODO update UAL?

        return response
    else:
        e = MethodNotAllowed(valid_methods=['PUT'],
                             description='PUT method expected')
        return e.get_response()


def plot(fignum=1):
    pl.ioff()
    pl.figure(fignum)
    pl.plot(pl.rand(100))
    pl.show()


def application(environ, start_response):
    request = Request(environ)
    response = dispatch_request(request)
    return response(environ, start_response)


if __name__ == '__main__':
    from werkzeug.serving import run_simple
    app = application
    run_simple('127.0.0.1', 5000, app, use_debugger=True, use_reloader=True)
