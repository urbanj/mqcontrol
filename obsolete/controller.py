from __future__ import division, print_function
import numpy as np
from cPickle import dumps, loads


def trapz(y0, y1, x0=0, x1=1):
    '''Trapezoidal rule for two fixed points y0(x0), y1(x1)
    '''
    return (y0 + y1) * (x1 - x0) / 2


def simple_deriv(y0, y1, x0=0, x1=1):
    '''Simple derivative
    '''
    return (y1 - y0) / (x1 - x0)


class ConstFunc(object):
    """A constant function"""
    def __init__(self, value):
        self.value = value

    def __call__(self, x=None):
        return self.value


class LinFunc(object):
    """Linear interpolation function for weights"""
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __call__(self, x):
        # numpy.interp uses constant extrapolation as required
        return np.interp(x, self.x, self.y)


class PID(object):
    """MIMO PID controller
    """

    __save_fields = {'K_p': 'K_p', 'K_i': 'K_i', 'K_d': 'K_d',
                     'err': 'err_integral', 'time': 'last_time'}

    def _make_K(self, K, set_shape=False, dtype=np.float_):
        if np.isscalar(K):
            Km = K * np.asmatrix(np.ones(1), dtype=dtype)
        else:
            Km = np.asmatrix(K, dtype=dtype)
            if Km is K:
                Km = Km.copy()
        if set_shape:
            self.shape = Km.shape
        else:
            assert Km.shape == self.shape
        return Km

    def K_p():
        doc = "The K_p property."
        def fget(self):
            return self._K_p
        def fset(self, value):
            self._K_p = self._make_K(value)
        def fdel(self):
            self._K_p = 0 * self._K_p
        return locals()
    K_p = property(**K_p())

    def K_i():
        doc = "The K_i property."
        def fget(self):
            return self._K_i
        def fset(self, value):
            self._K_i = self._make_K(value)
        def fdel(self):
            self._K_i = 0 * self._K_i
        return locals()
    K_i = property(**K_i())

    def K_d():
        doc = "The K_d property."
        def fget(self):
            return self._K_d
        def fset(self, value):
            self._K_d = self._make_K(value)
        def fdel(self):
            self._K_d = 0 * self._K_d
        return locals()
    K_d = property(**K_d())

    def __init__(self, K_p, K_i=0, K_d=0, err=0, time=None, integrator=trapz, derivator=simple_deriv):
        """

        :param K_p: K_p must always be properly shaped
        :param integrator: integration function f(y0, y1, x0, x1)
        """
        # super(PID, self).__init__()
        self._K_p = self._make_K(K_p, True)
        self.K_i = K_i
        self.K_d = K_d
        self.integrator = integrator
        self.derivator = derivator
        # signals are K * err
        if np.isscalar(err):
            self.last_err = np.asmatrix(err * np.ones((self.shape[1], 1)))
        else:
            if isinstance(err, np.ndarray):
                self.last_err = np.asmatrix(err).copy()
            else:
                self.last_err = np.asmatrix(err)
        self.last_time = time
        self.err_integral = 0
        # TODO: check shapes, functions etc.

    def get_state(self):
        '''Serialize state into a string'''
        state = {}
        for f, p in self.__save_fields.items():
            state[f] = getattr(self, p)
        return dumps(state)

    @classmethod
    def from_state(cls, state):
        '''Create from a saved state'''
        kwargs = loads(state)
        return cls(**kwargs)

    def step(self, time, err):
        '''Get response from one time step

        :param time: time
        :param err: error, must be properly shaped matrix (column vector)
        '''

        if self.last_time is None:
            # first call without an explicit initialization
            # --> take only the proportional term
            res = self.K_p*err
        else:
            assert time > self.last_time, "The time must go on!"

            # accumulate the integral error
            self.err_integral += self.integrator(self.last_err, err, self.last_time, time)
            # now the result can be calculated
            res = (self.K_p*err + self.K_i*self.err_integral +
                   self.K_d*self.derivator(self.last_err, err, self.last_time, time))

        self.last_time = time
        self.last_err = err

        return res


class Executor(object):
    """Executes controller(s) and provides actuator outputs from diagnostic inputs
    """

    def __init__(self, controllers):
        '''Initialize the executor

        All controllers must have the same input/output size (i.e. the same signal/actuator definition)

        :param controllers: list of controllers
        '''
        # super(ControlExecutor, self).__init__()
        self.controllers = [c for c in controllers]

    def step(self, time, signal, reference):
        # the output is the sum of particular controller contributions
        err = reference - signal
        res = sum((controller.step(time, err) for controller in self.controllers))
        return res


def main():
    from scipy.signal import lti, step, lsim
    import pylab as pl

    #
    # test case from http://ctms.engin.umich.edu/CTMS/index.php?example=Introduction&section=ControlPID
    #
    # sample plant transfer function
    plant = lti([1, ], [1, 10, 20])
    # pl.figure()
    # resp = step(plant)
    # pl.plot(resp[0], resp[1])
    # pl.title('plant step response')
    # constant reference

    def reference(time):
        return pl.matrix('1.0')

    # K_p-only PID
    # pid = PID([[300]])
    # PID gains
    pid = PID(300, 300, 10)
    # create an executor
    ex = Executor((pid, ))
    # simulate
    times = pl.linspace(0, 2, 1001)
    s = 0 * times
    control = 0 * times
    X = pl.zeros((plant.A.shape[0], 2))
    for i in xrange(1, times.size):
        control[i] = ex.step(times[i], s[i-1], reference(times[i]))
        # s[i] = plant.output(control[i], times[i])
        T, Y, X = lsim(plant, control[i-1:i+1], times[i-1:i+1], X[1])
        s[i] = Y[1]

        # print('---')
        # print(times[i-1:i+1])
        # print(control[i-1:i+1])
        # print(Y)

    pl.figure()
    pl.subplot(211)
    pl.plot(times, s)
    pl.grid('on')
    pl.ylabel('state')
    pl.subplot(212)
    pl.plot(times, control)
    pl.ylabel('cotrol')

    pl.show()

    return locals()


if __name__ == '__main__':
    main_result = main()
