from cpocontrol import *
import sys
import json

print('This is cpocontrol main')

# check whether we run for the first time
if 'session_data' not in locals() or not session_data:
    print('Init cpocontrol')
    session_data = {}
    session_data['config_file'] = inputs['config_file']
    print('config_file: %s' % session_data['config_file'])
    session_data['o_json'] = json.loads(open(inputs['config_file'], 'r').read())

    # set up sys.path
    for pp in session_data['o_json'].get('python_path', ()):
        sys.path.append(pp)
    # process imports
    for m in session_data['o_json']['imports']:
        exec('import %s' % m)
    # process manual inputs
    session_data['manual_inputs'] = {}
    for inp_name, inp_val in session_data['o_json'].get('manual_inputs', {}).items():
        session_data['manual_inputs'][inp_name] = eval(inp_val)
    # initialize controllers
    session_data['controllers'] = construct_controllers(session_data['o_json']['controllers'])
    # TODO this is just for debugging
    session_data[u'output_history'] = {u'time': [], u'exec': []}

# session_data initialized
time = eval(session_data['o_json']['time'])
manual_inputs = session_data['manual_inputs']

session_data[u'output_history'][u'time'].append(time)

print("locals: %s" % locals().keys())
print("outputs: %s" % outputs.keys())
# exec("outputs['pfsystems_out'].pfcoils.coilvoltage.value = numpy.arange(10)")

# execute the controller
for cname, contr in session_data['controllers'].items():
    print('# controller %s #' % cname)

    session_data[u'output_history'].setdefault(unicode(cname), [])

    step_argv = {}
    for inp_name, inp_val in contr['inputs'].items():
        # TODO specify the context explicitely
        step_argv[inp_name] = eval(str(inp_val), globals(), locals())
    # TODO references, feed_forward
    try:
        simout = contr['controller'].step(**step_argv)
    except Exception:
        print('Exception in controller %s.step' % cname)
        raise
    # TODO feed forward
    # u += feed_forward
    w = contr['weight'](time)
    # put output values to outputs (ie output ports)
    for out_name, out_val in contr['outputs'].items():
        print('%s = %s' % (out_name, out_val))
        try:
            session_data[u'output_history']['exec'].append('%s = %s' % (out_name, out_val))
            exec('%s = %s' % (out_name, out_val))
        except Exception:
            print('error in exec for %s' % out_name)
            raise

        dot_pos = out_name.find('.')
        # print('checking time attr of %s' % out_name[:dot_pos])
        if dot_pos > 0 and hasattr(eval(out_name[:dot_pos]), 'time'):
            print('%s.time = %g' % (out_name[:dot_pos], time))
            session_data[u'output_history']['exec'].append('%s.time = %g' % (out_name[:dot_pos], time))
            exec('%s.time = %g' % (out_name[:dot_pos], time))

        # dot_pos = out_name.find('.')
        # if dot_pos == -1:
        #     # TODO this is dangerous and might not work
        #     # locals()[outp] = val
        #     # this should work
        #     print('%s = %s' % (out_name, out_val))
        #     exec('%s = %s' % (out_name, out_val))
        # else:
        #     print('%s = %s (via setattr)' % (out_name, out_val))
        #     rec_setattr(locals()[out_name[:dot_pos]], out_name[dot_pos+1:], eval(out_val))
        #     # must change the time here
        #     if hasattr(locals()[out_name[:dot_pos]], 'time'):
        #         print('%s.time = %g' % (out_name[:dot_pos], time))
        #         setattr(locals()[out_name[:dot_pos]], 'time', time)

    session_data[u'output_history'][cname].append(dict(simout))

    # try:
    #     open('/tmp/ju_session_data.txt', 'w').write(str(session_data['output_history']))
    # except Exception:
    #     pass

    outputs['time_out'] = time
    # [outputs]['time_out'] = time
