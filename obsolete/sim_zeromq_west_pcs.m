function [CP, tatypeID, Complexity] = sim_zeromq_west_pcs(out_struct, fldname, portn)
% function [CP, tatypeID, Complexity] = sim_freebie_metis_pcs(equifree)
%  Output: without input parameters, the function gives 
%          [output_length, typeID, Complexity] of the simulink output port

    % the port lengths
    % the new Simulink
%     PORT_LENS = [1, 1, 1, 1, 1, 1, 1, 11];
    % the "old" Simulink controller
    PORT_LENS = ones(1, 11);
    PORT_LENS(11) = 11;
    % port data types, 0 for double
    PORT_TYPES = zeros(size(PORT_LENS));
    % port complexities
    PORT_CMPLXS = {};
    for i = 1:length(PORT_LENS)
        PORT_CMPLXS{i} = 'Real';
    end

    if nargin < 3
        portn = 1;
    end
    if nargin < 2
        fldname = [];
    end
    if nargin < 1
        out_struct = [];
    end

    if isempty(out_struct)
        % block initialization phase
        CP = PORT_LENS(portn);
        tatypeID = PORT_TYPES(portn);
        Complexity = PORT_CMPLXS{portn};
    else
        % simulation phase
        CP = out_struct.(fldname);
    end
end
