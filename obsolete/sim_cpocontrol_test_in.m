function [out] = sim_cpocontrol_test_in(out_struct, port_name, port_num)
% function [out] = sim_freebie_metis_pcs(equifree)
%  Output: with nargin = 0, the function gives a port description struct with
%     .dim
%     .DatatypeID
%     .Complexity
%  With nargin >= 2, the ouput is the actual numerical output for the
%  particular port name
%

out = struct();

if nargin == 0
    % port description
    inPorts = struct();
    inPorts.actuators.dim = 3;
    inPorts.actuators.DatatypeID = 0; % double
    inPorts.actuators.Complexity = 'Real';

    out = inPorts;
else
    % output for a specific port name
    out.test_out = out_struct.(port_name);
end

end
