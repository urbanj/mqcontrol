import zmq
import time
import numpy as np
import json

port = "5557"
context = zmq.Context()
socket = context.socket(zmq.PAIR)
socket.bind("tcp://*:%s" % port)

i = 0

while True:
    # msg = socket.recv()
    # e = 'error'
    # msg_out_json = json.dumps({"exception": "{}".format(e)})
    # socket.send(msg_out_json)

    i += 1
    msg = socket.recv()
    print('received:')
    print(msg)
    msg_in = json.loads(msg)
    print(msg_in)
    sim_struct = msg_in['sim_struct']

    out_struct = {}
    msg_out = {}
    try:
        out_struct["time"] = i * sim_struct['sim_time']
        out_struct["z"] = float(i)
        out_struct["ip"] = np.linalg.norm(sim_struct['repfcur'])

        msg_out['debug'] = 'testing python'
        msg_out['out_struct'] = out_struct
        msg_out_json = json.dumps(msg_out)
    except Exception as e:
        print(e)
        msg_out_json = json.dumps({"exception": "{}".format(e)})
    finally:
        print(msg_out_json)
        socket.send(msg_out_json)

    print("sleep {}".format(i))
    time.sleep(1)

