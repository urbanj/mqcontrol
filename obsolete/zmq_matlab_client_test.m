javaaddpath /pfs/work/jurban/jeromq/target/jeromq-0.3.6-SNAPSHOT.jar
import org.zeromq.ZMQ
addpath /pfs/home/jurban/itmwork/jsonlab

%%
% port = '5556';
% context = org.zeromq.ZMQ.context(1);
% socket = context.socket(org.zeromq.ZMQ.PAIR);
% socket.connect(['tcp://localhost:' port]);
zmq_connection.port = '5556';
zmq_connection.context = org.zeromq.ZMQ.context(1);
zmq_connection.socket = zmq_connection.context.socket(org.zeromq.ZMQ.PAIR);
% zmq_connection.socket.setSendTimeOut(500);
zmq_connection.socket.setReceiveTimeOut(500);
%% org.zeromq.setSocketOption(zmq_connection.socket, org.zeromq.ZMQ.ZMQ_RCVTIMEO, 500);
zmq_connection.socket.connect(['tcp://localhost:' zmq_connection.port]);

pfcur = complex([1e4, 2e4, -3e3]);
t = 1.1;
%%


%%
% while True:

    % - better serialize imaginary numbers
    % - automatically browse MetisFreebiePortInfo.in
    zmq_struct_send = struct();
    zmq_struct_send.sim_time = t;
    zmq_struct_send.repfcur = real(pfcur);
    zmq_struct_send.impfcur = imag(pfcur);
    zmq_msg = savejson('sim_struct', zmq_struct_send);
    disp(zmq_msg)
    % this is a non-blocking call
    zmq_connection.socket.send(zmq_msg);
    disp('sent')

    %%
    % blocking call
    % TODO add timeout
        while true
        zmq_resp = zmq_connection.socket.recv();
        if isempty(zmq_resp)
            disp('waiting for response')
            pause(5)
        else
            break
        end
    end

%     zmq_resp = zmq_connection.socket.recv();
    disp('received')
    zmq_struct_rcv = loadjson(char(zmq_resp.'))
    out_struct = zmq_struct_rcv.out_struct
       
%     
% 
%     % blocking call
%     msg = socket.recv();
%     char(msg.')
%     %%
%     socket.send('client message to server1');
%     socket.send('client message to server2');
%     socket.send('client message to server2x');
%     time.sleep(1)
% end
%%

% zmq_connection.socket.close()
zmq_connection.context.term()
 