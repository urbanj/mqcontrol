function [out] = sim_cpocontrol_test_out(out_struct, port_name, port_num)
% function [out] = sim_freebie_metis_pcs(equifree)
%  Output: with nargin = 0, the function gives a port description struct with
%     .dim
%     .DatatypeID
%     .Complexity
%  With nargin >= 2, the ouput is the actual numerical output for the
%  particular port name
%

if nargin == 0
    out = struct();
    
    % port description
    outPorts = struct();
    outPorts.test_out.dim = 2;
    outPorts.test_out.DatatypeID = 0; % double
    outPorts.test_out.Complexity = 'Real';

    outPorts.outport2.dim = 1;
    outPorts.outport2.DatatypeID = 0; % double
    outPorts.outport2.Complexity = 'Real';

    out = outPorts;
else
    % output for a specific port name
    out = out_struct.(port_name);
end

end
