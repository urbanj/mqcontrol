# -*- coding: utf-8 -*-
# import yaml
# import json
# from pprint import pprint
# import copy
import numpy as np
# import sys
from controller import PID, ConstFunc, LinFunc
from mqcontroller import ZMQController
# import os.path
# from pickle import dumps, loads, dump, load
from simupynk import Simupynk


def cpo_eval(obj):
    # print('cpo_eval(%s)' % obj)
    if isinstance(obj, dict):
        res = obj.copy()
        for k in obj:
            res[k] = cpo_eval(obj[k])
    elif isinstance(obj, (str, unicode)):
        print('eval(%s)' % obj)
        # res = lambda: eval(obj)
        res = eval(obj)
    else:
        try:
            res = map(cpo_eval, obj)
        except TypeError:
            # obj is not an iterator -> return obj itself
            res = obj
        else:
            pass
        finally:
            pass
    return res


def construct_controllers(controllers_dict):
    """Create a collection (dict) of controllers from an input dict
    """
    res = {}
    for name, config in controllers_dict.items():
        if 'PID' in config:
            # this is a standard PID controller
            gains = np.array(config['PID'])
            res[name] = dict(controller=PID(gains[:,:,0],
                             gains[:,:,1], gains[:,:,2]))
        elif 'simupynk' in config:
            res[name] = dict(controller=Simupynk(config['simupynk']))
            # TODO should we do init before inputs are available?
            res[name]['controller'].init()
        elif 'zmq' in config:
            res[name] = dict(controller=ZMQController(config['zmq']))
            # TODO should we do init before inputs are available?
            res[name]['controller'].init()
        else:
            raise Exception("Unknown controller type")
        res[name]['inputs'] = config.get('inputs', {})
        res[name]['references'] = config.get('references', {})
        res[name]['feed_forward'] = config.get('feed_forward', {})
        res[name]['outputs'] = config.get('outputs', {})
        config.setdefault('weight', 1)
        if isinstance(config['weight'], (float, int, long)):
            # just a constant
            w = float(config['weight'])
            res[name]['weight'] = ConstFunc(w)
        elif isinstance(config['weight'], (list, )):
            # this is a linear waveform
            if len(config['weight']) == 1:
                raise ValueError('at least two time points must be defined for weights'
                                 ' (or use just a single number)')
            w = np.array(config['weight'], dtype=np.float)
            res[name]['weight'] = LinFunc(w[:, 0], w[:, 1])
        else:
            # an expression
            res[name]['weight'] = lambda t: eval(config['weight'])

    return res


def eval_as_array(expr, glob, loc):
    '''Eval an expression in a given context as a numpy array
    '''
    val = eval(expr, glob, loc)
    val = np.asarray(val).reshape((-1, ))
    return val


def rec_setattr(obj, attr, value):
    """Set object's attribute. May use dot notation.

    >>> class C(object): pass
    >>> a = C()
    >>> a.b = C()
    >>> a.b.c = 4
    >>> rec_setattr(a, 'b.c', 2)
    >>> a.b.c
    2
    """
    if '.' not in attr:
        setattr(obj, attr, value)
    else:
        L = attr.split('.')
        rec_setattr(getattr(obj, L[0]), '.'.join(L[1:]), value)

