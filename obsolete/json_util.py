import json


def json_src_escape(src):
    """Escape a Python source code for JSON serialization
    """
    # src = src.replace('\\', '\\\\')
    # src = src.replace('"', '\\"')
    return src


def dumps_src(src):
    """Dumps into a JSON string using a {'code': src} dict
    """
    return json.dumps({'code': json_src_escape(src)})


def loads_src(s):
    """Loads a source code dumped by dumps_src
    """
    return json.loads(s)['code']
