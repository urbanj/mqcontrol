from json_util import *
from json import dumps, loads


def process_json(src):
    return loads_src(dumps_src(src))


def test_simple():
    src = "True"
    assert eval(process_json(src))


def test_backslash():
    src = """abs \
(-1)"""
    assert eval(process_json(src)) == 1


def test_indent():
    src = """
if True:
    a_var_ = 1
"""
    exec(process_json(src))
    assert locals()['a_var_'] == 1


def test_single_quotes():
    src = "'s'"
    assert eval(process_json(src)) == "s"


def test_double_quotes():
    src = '"d"'
    assert eval(process_json(src)) == "d"


def test_print1():
    src = 'print(\'"\\\'\')'
    exec(process_json(src))


def test_print2():
    src = 'print(\"\\\"\'\")'
    exec(process_json(src))

