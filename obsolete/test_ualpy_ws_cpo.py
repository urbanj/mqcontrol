import requests
import json
from nose import with_setup


shot = 2000
run = 9908
idx = 0


def init():
    requests.get('http://localhost:5000/init/%i/%i/%i' %
                 (shot, run, idx))

def finish():
    requests.get('http://localhost:5000/finish')

ini_fini = with_setup(init, finish)


@ini_fini
def test_put_cpo():
    cpo = {
    'cpo': 'equilibrium',
    'occurrence': 0,
    'time': 0,
    }
    headers = {'content-type': 'application/json'}
    r = requests.put('http://localhost:5000/put_cpo',
                     data=json.dumps(cpo),
                     headers=headers)
    assert r.ok

    r = requests.get('http://localhost:5000/context')
    assert 'equilibrium0' in r.json()['ual']
