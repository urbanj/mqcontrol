# -*- coding: utf-8 -*-
# import yaml
import json
from pprint import pprint
# import copy
import numpy as np
import sys
sys.path.append('/pfs/work/jurban/cpocontrol')
from controller import PID, ConstFunc, LinFunc
import os.path
from pickle import dumps, loads, dump, load
# import number


def cpo_eval(o):
    # print('cpo_eval(%s)' % o)
    if isinstance(o, dict):
        r = o.copy()
        for k in o:
            r[k] = cpo_eval(o[k])
    elif isinstance(o, (str, unicode)):
        print('eval(%s)' % o)
        # r = lambda: eval(o)
        r = eval(o)
    else:
        try:
            r = map(cpo_eval, o)
        except TypeError:
            # o is not an iterator -> return o itself
            r = o
        else:
            pass
        finally:
            pass
    return r


def construct_controllers(controllers_dict):
    """Create an collection (dict) of controllers from an input dict
    """
    res = {}
    for name, config in controllers_dict.items():
        if 'PID' in config:
            # this is a standard PID controller
            gains = np.array(config['PID'])
            res[name] = dict(controller=PID(gains[:,:,0],
                             gains[:,:,1], gains[:,:,2]))
        else:
            raise Exception("Unknown controller type")
        res[name]['inputs'] = config['inputs']
        res[name]['references'] = config['references']
        res[name]['feed_forward'] = config['feed_forward']
        res[name]['outputs'] = config['outputs']
        if isinstance(config['weight'], (float, int, long)):
            # just a constant
            w = float(config['weight'])
            res[name]['weight'] = ConstFunc(w)
        elif isinstance(config['weight'], (list, )):
            # this is a linear waveform
            if len(config['weight']) == 1:
                raise ValueError('at least two time points must be defined for weights'
                                 ' (or use just a single number)')
            w = np.array(config['weight'], dtype=np.float)
            res[name]['weight'] = LinFunc(w[:, 0], w[:, 1])
        else:
            # an expression
            res[name]['weight'] = lambda t: eval(config['weight'])

    return res


def eval_as_array(expr, glob, loc):
    '''Eval an expression in a given context as a numpy array
    '''
    val = eval(expr, glob, loc)
    val = np.asarray(val).reshape((-1, ))
    return val


def rec_setattr(obj, attr, value):
    """Set object's attribute. May use dot notation.

    >>> class C(object): pass
    >>> a = C()
    >>> a.b = C()
    >>> a.b.c = 4
    >>> rec_setattr(a, 'b.c', 2)
    >>> a.b.c
    2
    """
    if '.' not in attr:
        setattr(obj, attr, value)
    else:
        L = attr.split('.')
        rec_setattr(getattr(obj, L[0]), '.'.join(L[1:]), value)


def test_simple_evolution(Ip_ref=1.0, Z_ref=1.0, config_file='test_simple_evolution.json', state_file='test_state'):
    from scipy.signal import lti, lsim
    from numpy import zeros_like, linspace, zeros, asmatrix
    # this is the first run --> open config, create controllers
    print('opening config: %s' % config_file)
    with open(config_file, 'r') as f_conf:
        conf = json.load(f_conf)
    print('Parsed JSON config:')
    pprint(conf)

    # model z axis using lti
    z_ax_lti = lti([1, ], [1, 10, 20])
    # initial state
    X0 = zeros_like(z_ax_lti.A)
    # simulation times
    sim_times = linspace(0, 2, 1001)
    # sim_times = linspace(0, 1e-3, 5)
    sim_control = 0 * sim_times
    sim_signal = 0 * sim_times
    sim_X = zeros(sim_times.shape + X0.shape)
    sim_X[0] = X0

    # inputs as a dict
    def get_inputs(i, control):
        res = {}
        equi_cpo = {}
        # just a constant Ip
        equi_cpo['Ip'] = Ip_ref
        # print('lsim')
        # print(control[i-1:i+1])
        # print(sim_times[i-1:i+1])
        # print(sim_X[i-1,1])

        T, Y, X = lsim(z_ax_lti, control[i-1:i+1], sim_times[i-1:i+1], sim_X[i-1,1])
        # print(Y)
        sim_X[i] = X
        equi_cpo['Z'] = Y[1]
        res['equi_cpo'] = equi_cpo
        return res

    def get_references(i):
        res = {}
        equi_ref = {}
        equi_ref['IpZ'] = Z_ref * Ip_ref
        res['equi_ref'] = equi_ref
        return res

    def get_outputs(i):
        res = {}
        res['pf'] = {}
        res['pf']['voltage'] = zeros(3)

    for i, t in enumerate(sim_times):
        if i == 0:
            controllers = construct_controllers(conf['controllers'])
            # executor = Executor(controllers.values())
            for contr in controllers.values():
                print(contr['controller'].K_p)
                print(contr['controller'].K_i)
                print(contr['controller'].K_d)
        else:
            with open(state_file, 'r') as sfile:
                # executor = load(sfile)
                controllers = load(sfile)
            for contr in controllers.values():
                context = {}
                context.update(get_references(i))
                reference = asmatrix([eval(s, globals(), context) for s in contr['references']])
                err = reference - sim_signal[i-1]
                # print('s = %g' % sim_signal[i-1])
                # print('err = %g' % err)
                sim_control[i] = contr['controller'].step(t, err) / Ip_ref
                # print('c = %g' % sim_control[i])
                context.update(get_inputs(i, sim_control))
                signal = asmatrix([eval(s, globals(), context) for s in contr['inputs']])
                # works for a single controller only
                # PROBLEM: time flow ???
            sim_signal[i] = signal
        with open(state_file, 'w') as sfile:
            # dump(executor, sfile)
            dump(controllers, sfile)

    return sim_times, sim_control, sim_signal


def plot_simple_test():
    import matplotlib.pyplot as pl
    # set Ip here
    Ip_ref = 2e5
    Z_ref = 0.2

    sim_times, sim_control, sim_signal = test_simple_evolution(Ip_ref, Z_ref)

    pl.figure()
    pl.subplot(211)
    pl.plot(sim_times, sim_signal)
    pl.axhline(Ip_ref * Z_ref, c='r', ls='--')
    pl.grid('on')
    pl.ylabel('state')
    pl.subplot(212)
    pl.plot(sim_times, sim_control)
    pl.ylabel('cotrol')
    pl.show()


if __name__ == '__main__':

# check whether this is UALPython
    try:
        # test whether cpo_in is available
        cpo_in
        config_file = '/pfs/work/jurban/cpocontrol/xample.json'
        pickle_file = '/pfs/work/jurban/cpocontrol/c_pickle'
    except Exception, e:
        # use the example
        # execfile('ualpython_xample.py')
        config_file = 'xample_control.json'
        pickle_file = 'c_pickle'
        # raise e
    finally:
        pass

    if os.path.isfile(pickle_file):
        print("unpickle from %s" % pickle_file)
        pickle_str = open(pickle_file, 'r').read()
    else:
        pickle_str = ''

    print('opening config: %s' % config_file)
    # t_json = ''.join(open(config_file, 'r').readlines())
    # dec = json.JSONDecoder()
    o_json = json.loads(open(config_file, 'r').read())
    print('JSON:')
    pprint(o_json)
    for m in o_json['imports']:
        m_split = m.split('.')
        m = locals()[m_split[0]] = __import__(m)
        for dotm in m_split[1:]:
            setattr(m, dotm, __import__(m))
            m = getattr(m, dotm)

    time = eval(o_json['time'])

    if pickle_str:
        controllers = loads(pickle_str)
    else:
        controllers = construct_controllers(o_json['controllers'])

    # execute the controller
    for cname, contr in controllers.items():
        print('# controller %s #' % cname)

        inputs = np.asmatrix(np.hstack([eval_as_array(str(s), globals(), locals()) for s in contr['inputs']]))
        reference = np.asmatrix(np.hstack([eval(str(s), globals(), locals()) for s in contr['references']]))
        feed_forward = np.asmatrix(np.hstack([eval(str(s), globals(), locals()) for s in contr['feed_forward']]))
        err = reference - inputs
        u = contr['controller'].step(time, err.T)
        u += feed_forward
        w = contr['weight'](time)
        print('inputs:')
        print(str(inputs))
        print('reference:')
        print(str(reference))
        print('weight:')
        print(str(w))
        print('error:')
        print(str(err))
        print('control:')
        print(str(u))
        # u should be a columnt vector of outputs
        u = w * np.asarray(u[:, 0]).reshape(-1)
        # for outp, val in zip(contr['outputs'], u):
        for i, outp in enumerate(contr['outputs']):
            val = u[i:i+1]
            dot_pos = outp.find('.')
            if dot_pos == -1:
                # TODO this is dangerous and might not work
                # locals()[outp] = val
                # this should work
                print('%s = %s' % (outp, val))
                exec('%s = %g' % (outp, val))
            else:
                print('%s = %s' % (outp, val))
                rec_setattr(locals()[outp[:dot_pos]], outp[dot_pos+1:], val)
                # must change the time here
                if hasattr(locals()[outp[:dot_pos]], 'time'):
                    print('%s.time = %g' % (outp[:dot_pos], time))
                    setattr(locals()[outp[:dot_pos]], 'time', time)

    # save the last state
    # pickle to a string
    pickle_str = dumps(controllers)
    # save to a file
    # TODO send to on output port
    with open(pickle_file, 'w') as f:
        f.write(pickle_str)

