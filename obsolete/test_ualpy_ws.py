import requests
import json
from nose import with_setup

def init():
    requests.get('http://localhost:5000/init')

def finish():
    requests.get('http://localhost:5000/finish')

ini_fini = with_setup(init, finish)


def test_root():
    r = requests.get('http://localhost:5000')
    assert r.text.strip().split()[0] == 'UALPython-WS'

def test_init_finish():
    r = requests.get('http://localhost:5000/init')
    assert r.ok
    r = requests.get('http://localhost:5000/finish')
    assert r.ok

@ini_fini
def test_context():
    r = requests.get('http://localhost:5000/context')
    assert r.ok
    assert r.json()['init']

@ini_fini
def test_put():
    data = {'some': 'data', 'another': 1}
    data['list'] = [1, 'a', -1e-3]
    headers = {'content-type': 'application/json'}
    for key, value in data.items():
        r = requests.put('http://localhost:5000/put',
                         data=json.dumps({key: value}),
                         headers=headers)
    r = requests.get('http://localhost:5000/context')
    print('context: \n%s' % r.json())
    assert r.json()['data'] == data

@ini_fini
def test_exec_out():
    code = """
a = 2
"""
    data = {'code': code,
            'output' : ['a']}
    headers = {'content-type': 'application/json'}
    r = requests.put('http://localhost:5000/exec',
                     data=json.dumps(data), headers=headers)
    assert r.ok
    r = requests.get('http://localhost:5000/context')
    assert r.json()['data']['a'] == 2

@ini_fini
def test_exec_print():
    code = """
print(\'"\\\'\')
print(\"\\\"\'\")
"""
    data = {'code': code}
    headers = {'content-type': 'application/json'}
    r = requests.put('http://localhost:5000/exec',
                     data=json.dumps(data), headers=headers)
    assert r.ok
    print r.text
    assert r.text == '"\'\n"\'\n'

