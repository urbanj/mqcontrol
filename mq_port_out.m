function [out] = mq_port_out(out_struct, port_name, port_num)
% function [out] = sim_freebie_metis_pcs(equifree)
%  The ouput is the actual numerical output for the
%  particular port name
%

% output for a specific port name
out = out_struct.(port_name);

end
