# MQControl

The MQControl project implements a ligh-weight, asynchronous interface between Simulink and other languages (e.g. Python) via a message queue / message broker system, in particular Redis. This enables running concurrent simulations in Simulink GUI and Python. These simulations can run on different systems and cummunicate via the Internet.

The interface is configured via a JSON file, which contains the ports descriptions and the plant function (in Python). This plant function serves as a combination of actuators and synthetic diagnostics, which are input and output to a plant simulator. The plant simulator can be anything that is run from Python, e.g. a Python code, a Fortran function via f2py or C-API or a stand-alone executable.

## Getting started

### Requirements
* Matlab + Simulink
* [JSONlab](https://www.mathworks.com/matlabcentral/fileexchange/33381-jsonlab--a-toolbox-to-encode-decode-json-files)
* A Redis client for Matlab, e.g. [go-redis](https://github.com/markuman/go-redis)
* Python with Redis client installed

### JSON config
```JSON
{
    "python_path": ["/list/of/paths", "/to/be/added/to/sys.path"],
    "imports": ["my_plant_module", "to_be_imported"],
    "interface": {
        "type": "redis",
        "host": "localhost",
        "port": 6379
    },
    # port from simulink to the plant simulator
    "to_simulink": {
        "port_name_1": {
            "dim": port_dimension,
            "type": "double"
        },
        "port_name_2": {
            "dim": port_dimension,
            "type": "double"
        }
    },
    # ports from the plant simulator to simulink
    "from_simulink": ["input_1", "anther_input_2"],
    "plant_function": "my_plant_module.my_plant_simulator"
}
```
*Attention:* #comments are not allowed in JSON --> delete them before using this as a template.

### Testing
1. Start a Redis server, either on the local machine or provide tunnels: 
```
redis-server --port 6379
```
2. Open and run mq_test_loop in Simulink (do not forget to `addpath` JSONlab and go-redis).
3. At the same time, run 
```
python test_mq_redis.py test_cpocontrol_full.json
```
4. You should see a similar output as in `test_mq_redis_out.txt`

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details.
