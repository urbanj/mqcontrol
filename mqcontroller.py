import json
import numpy as np
from time import sleep


class ZMQController(object):
    """Controller using ZMQ communication, especially for Simulink"""

    def __init__(self, uri):
        import zmq

        super(ZMQController, self).__init__()
        self.uri = uri
        # TODO init ZMQ
        self.context = zmq.Context()
        self.socket = self.context.socket(zmq.PAIR)
        self.socket.bind(uri)

    def init(self):
        pass

    def step(self, time=None, **inputs):
        """Single time step of the controller
        """
        # in case of any exception, send back an error message
        msg_out_json = json.dumps({"exception": "unknown"})
        try:
            # Receive the current controller output
            print("before recv")
            msg_in = self.socket.recv()
            print('Received:\n%s' % msg_in)
            # parse json
            msg_in_json = json.loads(msg_in)
            # everything from Simulink is in sim_struct
            sim_struct = msg_in_json['sim_struct']
            # catch exception from Simulink
            if "exception" in sim_struct:
                raise Exception(sim_struct["exception"])

            msg_out = {}

            # out_struct containt tokamak model outputs
            msg_out["time"] = time
            out_struct = dict(((k, for_json(v)) for (k, v) in inputs.items()))

            msg_out['out_struct'] = out_struct
            msg_out['debug'] = 'From Python / ZMQController'
            msg_out_json = json.dumps(msg_out)
        except Exception as e:
            print('Exception: %s' % e)
            msg_out_json = json.dumps({"exception": "{}".format(e)})
        finally:
            print("Controller message:\n%s" % msg_out_json)
            self.socket.send(msg_out_json)
            print("--sent\n")

        # return the input port values of the tokamak (plant) block in Simulink
        return sim_struct


class RedisController(object):
    """Controller using Redis communication, especially for Simulink"""

    def __init__(self, host='localhost', port=6379, simid="", **kwargs):
        import redis

        super(RedisController, self).__init__()
        self.r = redis.Redis(host=host, port=port, **kwargs)
        # self.pubsub.subscribe('from.controller')
        if not simid:
            # no simid ==> pop the last one
            print('No simulation id --> trying to get it from Redis cpocontrol.redis.id')
            simid = self.r.brpop('cpocontrol.redis.id')
            simid = simid[1]
            print('Received cpocontrol id: {}'.format(simid))
        self.channel_in = '{}:from.simulink'.format(simid)
        self.channel_out = '{}:to.simulink'.format(simid)
        # pop ping time in seconds
        self.ping_time = 0.01
        # Matlab does not support pubsub easily
        # self.pubsub = self.r.pubsub()
        # self.pubsub.subscribe(self.channel_in)

    def init(self):
        pass

    def get(self):
        """Single time step of the controller
        """

        try:

            # TODO async controller now?
            # - do not call the cotroller until time is lower than the last controller time (sim_time)
            # - for example,use self.state to save the last controller state (output)
            # Receive the current controller output
            print("before recv")
            # pubsub
            # for msg_in in self.pubsub.listen():
            #     # wait for the right message
            #     if msg_in['type'] == 'message' and msg_in['channel'] == self.channel_in:
            #         msg_in = msg_in['data']
            #         break
            # lpush / rpop
            # while True:
            #     # wait for the right message
            #     msg_in = self.r.rpop(self.channel_in)
            #     if msg_in:
            #         break
            # wait for the right message
            msg_in = self.r.brpop(self.channel_in)
            msg_in = msg_in[1]

            print('Received:\n%s' % msg_in)
            # parse json
            msg_in_json = json.loads(msg_in)
            # everything from Simulink is in sim_struct
            sim_struct = msg_in_json['sim_struct']
            # catch exception from Simulink
            if "exception" in sim_struct:
                print('exception received: %s' % sim_struct["exception"])
                raise Exception(sim_struct["exception"])
        except Exception as e:
            # in case of any exception, send back an error message
            msg_out_json = json.dumps({"exception": str(e)})
            self.r.lpush(self.channel_out, msg_out_json)
            raise

        # return the input port values of the tokamak (plant) block in Simulink
        return sim_struct

    def put(self, time, **inputs):
        try:
            msg_out = {}

            # out_struct containt tokamak model outputs
            msg_out["time"] = time
            out_struct = dict(((k, for_json(v)) for (k, v) in inputs.items()))

            msg_out['out_struct'] = out_struct
            msg_out['debug'] = 'From Python / RedisController'
            msg_out_json = json.dumps(msg_out)
        except Exception as e:
            print('Exception: %s' % e)
            msg_out_json = json.dumps({"exception": "{}".format(e)})
        finally:
            print("Controller message:\n%s" % msg_out_json)
            # self.r.publish(self.channel_out, msg_out_json)
            self.r.lpush(self.channel_out, msg_out_json)
            print("--sent\n")

    def step(self, time, **inputs):
        self.put(time, **inputs)
        sim_struct = self.get()
        return sim_struct


def for_json(value):
    if isinstance(value, (np.ndarray, np.generic)):
        res = value.tolist()
    else:
        res = value

    return res
