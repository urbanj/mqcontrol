def simulate(sim_time, sim_out_ports):
    """A test plant model
    """
    inputs = {}
    inputs['test_out'] = [sim_out_ports['in_2'][0] + sim_time,
                          sim_out_ports['in_2'][1] + sim_time]
    inputs['outport2'] = [sim_out_ports['in_1']]

    return inputs
