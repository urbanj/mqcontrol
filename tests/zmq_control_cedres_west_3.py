import pyual
import actors
import numpy as np
from mqcontroller import RedisController
import cedres_sim_outputs
from time import sleep
from copy import deepcopy
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("-p", "--port", type=int, default=6379,
                    help="controller port")

args = parser.parse_args()

# first equilibrium
# -----
itm = pyual.ITM(100, 3, machine='test')
eqs_in = itm.get('equilibrium')
pfs_in = itm.get('pfsystems')
wall_in = itm.get('wall', time=0)
iron_in = itm.get('ironmodel', time=0)

codeparams = './west_params.xml'

# cp_lines = open('cedres_params.xml', 'r').readlines()
# codeparams = ''.join(cp_lines)

outputs = []

eq_in = eqs_in[0]
pf_in = pfs_in[0]
eq0 = actors.cedres(eq_in, pf_in, wall_in, iron_in, codeparams=codeparams, dbgmode=True)
eq0 = eq0[0]

# eq0 = actors.cedres_raw(eq_in, pfs_in, wall_in, iron_in, codeparams=codeparams, dbgmode=True)

outputs.append((eq0, pf_in))

codeparams_rec = './west_params_rec.xml'

eq_in = eqs_in[1]
pf_in = pfs_in[1]
rec_out = actors.cedresrec(eq_in, eq0, pf_in, wall_in, iron_in,
                           codeparams=codeparams_rec, dbgmode=True)

eq_out = rec_out[1][0]
pf_out = rec_out[0][0]

outputs.append((eq_out, pf_out))
# -----

# initialize the Simulink controller
# port = "5557"
# zcont = ZMQController("tcp://*:%s" % port)
zcont = RedisController(port=args.port)

# define input names for the controller, will translate to outN
# input_names = ['Ne', 'Pr_tot', 'Zeff', 'Prad_ratio', 'Ip', 'Fp',
#                'I_coils', 'V_plasma', 'Ra', 'Za', 'Rext', 'Zdown']
input_names = ['Fp', 'R_axis', 'Z_axis', 'R_ext', 'R_int', 'Z_up',
               'Z_down', 'V_plasma', 'a', 'kaapa', 'I_coils']


def get_sim_inputs(equilibrium, pfsystems):
    inputs = {}
    for i, iname in enumerate(input_names):
        func = getattr(cedres_sim_outputs, iname)
        inputs['out%d' % (i + 1)] = func(equilibrium=equilibrium, pfsystems=pfsystems)

    return inputs


# first inputs for the controller
sim_in = [get_sim_inputs(eq_out, pf_out)]

# call the controller
t = eq0.time
sim_out = [zcont.step(time=t, **sim_in[-1])]
print("sim_out:\n%s" % sim_out[-1])

dt = 1e-3
times = np.arange(t + dt, 10 * dt, dt)

for t in times:
    eq_in = deepcopy(outputs[-1][0])
    pf_in = deepcopy(outputs[-1][1])
    eq_in.time = t
    pf_in.time = t
    pf_in.pfsupplies.voltage.value[:] = sim_out[-1]['in_struct']['vcoils']
    # eq_out = actors.cedresevo_raw(eq, pfs, wall, iron, dt, codeparams=codeparams, dbgmode=True)
    evo_out = actors.cedresevo(eq_in, pf_in, wall_in, iron_in, dt, codeparams=codeparams_rec, dbgmode=True)
    eq_out = evo_out[1][0]
    pf_out = evo_out[0][0]
    outputs.append((eq_out, pf_out))

    # inputs = get_sim_inputs(eq_out, pf_out)
    sim_in.append(get_sim_inputs(eq_out, pf_out))
    # call the controller
    sim_out.append(zcont.step(time=t, **sim_in[-1]))
    print("sim_out:\n%s" % sim_out[-1])
    sleep(2)
