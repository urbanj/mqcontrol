import pydons
import numpy as np


def test_simupynk():
    inputs = {}
    inputs['config_file'] = '/u/jurban/itmwork/cpocontrol/xample_simupynk.json'
    # mat_in = pydons.hdf5storage.loadmat('/pfs/work/jurban/tcv_controller/tcv_control_simupynk/test_input.mat')
    # inputs['mat_in'] = mat_in

    res = pydons.MatStruct()

    context = {}
    context.update(inputs)
    context['inputs'] = inputs
    context['session_data'] = {}

    for time_in in np.arange(0, 1e-3, 1e-4):

        outputs = {}
        outputs['Out1'] = {}
        outputs['Out2'] = {}
        # time_in = 0.0

        context['time_in'] = time_in
        context['outputs'] = outputs

        # exec(open('cpocontrol.py', 'w').read(), globals(), context)
        exec(open('cpocontrol_main.py', 'r').read(), context)

        # print(outputs)
        for f in outputs:
            if f not in res:
                res[f] = outputs[f]
            else:
                res[f] = np.vstack((res[f], outputs[f]))

    return res

if __name__ == '__main__':
    res = test_simupynk()
    res.savemat('test_simupynk_res.mat')
