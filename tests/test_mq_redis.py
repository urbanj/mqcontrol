import numpy as np
from mqcontroller import RedisController
import argparse
import json
import sys
import os
import importlib


parser = argparse.ArgumentParser()
parser.add_argument('cfg_file', type=str, nargs=1,
                    help='JSON configuration file path')

args = parser.parse_args()

cfg = json.load(open(args.cfg_file[0], 'r'))

if cfg['interface']['type'].lower() == 'redis':
    # initialize the Simulink controller
    mq_cont = RedisController(host=cfg['interface']['host'], port=cfg['interface']['port'])

for path in cfg['python_path']:
    sys.path.append(os.path.abspath(path))
for name in cfg['imports']:
    importlib.import_module(name)

func_parts = cfg['plant_function'].split('.')
if len(func_parts) == 1:
    plant_function = globals()[func_parts[-1]]
else:
    plant_function = getattr(sys.modules['.'.join(func_parts[:-1])], func_parts[-1])

while True:

    sim_out = mq_cont.get()
    print("sim_out:\n%s" % sim_out)

    if 'terminate' in sim_out['status'].lower():
        # Simulink stops the simulation
        break

    sim_time = sim_out['sim_time']
    print('Simulink time: {}'.format(sim_time))

    # get the system state
    sim_in = plant_function(sim_time, sim_out['in_struct'])
    print("sim_in:\n%s" % sim_in)
    # send to the controller
    mq_cont.put(time=sim_time, **sim_in)
