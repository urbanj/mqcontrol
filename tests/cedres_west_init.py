import pyual
import actors

# first equilibrium
# -----
itm = pyual.ITM(100, 3, machine='test', user='g2cboulb')
eqs_in = itm.get('equilibrium')
pfs_in = itm.get('pfsystems')
wall_in = itm.get('wall', time=0)
iron_in = itm.get('ironmodel', time=0)

codeparams = './west_params.xml'

eq_in = eqs_in[0]
pf_in = pfs_in[0]
eq0 = actors.cedres(eq_in, pf_in, wall_in, iron_in, codeparams=codeparams, dbgmode=True)
# Python actor returns an array of CPOs
eq0 = eq0[0]

codeparams_rec = './west_params_rec.xml'

eq_in = eqs_in[1]
pf_in = pfs_in[1]
rec_out = actors.cedresrec(eq_in, eq0, pf_in, wall_in, iron_in,
                           codeparams=codeparams_rec, dbgmode=True)

eq_out = rec_out[1][0]
pf_out = rec_out[0][0]

