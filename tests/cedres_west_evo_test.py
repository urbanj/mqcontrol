import pyual
import actors
import numpy as np
from copy import deepcopy

# first equilibrium
# -----
itm = pyual.ITM(100, 3, machine='test', user='g2cboulb')
eqs_in = itm.get('equilibrium')
pfs_in = itm.get('pfsystems')
wall_in = itm.get('wall', time=0)
iron_in = itm.get('ironmodel', time=0)

codeparams = './west_params.xml'

eq_in = eqs_in[0]
pf_in = pfs_in[0]
eq0 = actors.cedres(eq_in, pf_in, wall_in, iron_in, codeparams=codeparams, dbgmode=True)
# Python actor returns an array of CPOs
eq0 = eq0[0]

# store all outputs - can be disabled later
outputs = []
# each item is (equilibrium pfsystems)
outputs.append((eq0, None))

codeparams_rec = './west_params_rec.xml'

eq_in = eqs_in[1]
pf_in = pfs_in[1]
rec_out = actors.cedresrec(eq_in, eq0, pf_in, wall_in, iron_in,
                           codeparams=codeparams_rec, dbgmode=True)

eq_out = rec_out[1][0]
pf_out = rec_out[0][0]

outputs.append((eq_out, pf_out))

# set up time
t = eq0.time
dt = 1e-3
times = np.arange(t + dt, 2 * dt, dt)

for t in times:
    # start from the previous state
    eq_in = deepcopy(outputs[-1][0])
    pf_in = deepcopy(outputs[-1][1])
    eq_in.time = t
    pf_in.time = t
    # zero voltage
    pf_in.pfsupplies.voltage.value[:] = 0
    # eq_out = actors.cedresevo_raw(eq, pfs, wall, iron, dt, codeparams=codeparams, dbgmode=True)
    evo_out = actors.cedresevo(eq_in, pf_in, wall_in, iron_in, dt, codeparams=codeparams_rec, dbgmode=True)
    eq_out = evo_out[1][0]
    pf_out = evo_out[0][0]
    outputs.append((eq_out, pf_out))
