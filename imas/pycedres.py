import numpy as np


def cpos2simulink(equi, pf):
    to_simulink = {}
    ts = equi.time_slice[0]
    to_simulink["Phi_B"] = ts.global_quantities.psi_boundary
    to_simulink["Magnetic_axis"] = [ts.global_quantities.magnetic_axis.r, ts.global_quantities.magnetic_axis.z]
    to_simulink["Geometric_axis"] = [ts.boundary.geometric_axis.r, ts.boundary.geometric_axis.z]
    x_points = np.zeros((2, 2))
    for i, xpt in enumerate(ts.boundary.x_point):
        # TODO check transpose
        x_points[i] = [xpt.r, xpt.z]
    to_simulink["X_point"] = x_points.tolist()
    to_simulink["Minor_Radius"] = ts.boundary.minor_radius
    to_simulink["Elongation"] = ts.boundary.elongation
    to_simulink["R_out"] = ts.boundary.outline.r.max()
    # TODO dX = Lower divertor- X point gap
    to_simulink["dX"] = 0
    to_simulink["V_plasma"] = ts.global_quantities.volume
    to_simulink["li"] = ts.global_quantities.li_3

    n_sepa = 64
    # assume a single boundary contour
    r_sepa = ts.boundary.outline.r
    z_sepa = ts.boundary.outline.z
    # interpolate dR, dZ (theta)
    dR = r_sepa - ts.global_quantities.magnetic_axis.r
    dZ = z_sepa - ts.global_quantities.magnetic_axis.z
    theta = np.arctan2(dR, dZ)
    i_sort = np.argsort(theta)
    theta_i = np.linspace(theta.min(), theta.max(), n_sepa)
    r_i = np.interp(theta_i, theta[i_sort], dR[i_sort]) + ts.global_quantities.magnetic_axis.r
    z_i = np.interp(theta_i, theta[i_sort], dZ[i_sort]) + ts.global_quantities.magnetic_axis.z
    to_simulink["Shape"] = np.ravel([r_i, z_i], 'F').tolist()
    # get all pf coil currents
    pf_currents = [coil.current.data[0] for coil in pf.coil]
    assert len(pf_currents) == 17
    # these currents should be the same (coils in series)
    I_Xh = sum(pf_currents[9:13]) / 4  # (I_Xb1+I_Xb2+I_Xb3+I_Xb4)/4;
    I_Xb = sum(pf_currents[13:]) / 4  # I_Xh=(I_Xh1+I_Xh2+I_Xh3+I_Xh4)/4;
    # the controller expects circuit currents, not individual coil currents
    to_simulink["I_coils"] = (pf_currents[0:9] + [I_Xb, I_Xh])

    # TODO
    # pfpassive.current.toroidal.value is empty for the moment
    to_simulink["I_passive_structure"] = [0, 0]
    to_simulink["B_sensors"] = np.zeros((110, )).tolist()
    to_simulink["Flux_loops"] = np.zeros((17, )).tolist()

    return to_simulink


def simulink2cpos(sim_time, sim_out_ports, cpos):
    # Simulink ports
    # ["Ip", "Alpha", "Beta", "Gamma", "Rzero", "Rtoro", "Btoro", "P_boundary", "V_coils", "Invessel_coils_state", "Plasma_state"]
    # V_coils[10] = Xh
    # V_coils[11] = Xb
    # pf_in.pfsupplies.desc_supply.name
    #     ['G0 ', 'G1h', 'G2h', 'G3h', 'G4h', 'G4b', 'G3b', 'G2b', 'G1b', 'GXh', 'GXb']
    cpos['equilibrium'].time_slice[0].global_quantities.ip = sim_out_ports['Ip']
    # for coil, vcoil in zip(cpos['pf_active'].coil, sim_out_ports['V_coils']):
    #     coil.voltage.data[0] = vcoil
    assert len(cpos['pf_active'].supply) == len(sim_out_ports['V_coils'])
    for supply, V_in in zip(cpos['pf_active'].supply, sim_out_ports['V_coils']):
        supply.voltage.data[0] = V_in
    return cpos
