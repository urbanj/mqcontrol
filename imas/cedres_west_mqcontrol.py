import pyual
import actors
import numpy as np
from copy import deepcopy
from mqcontroller import RedisController
import pycedres
import json


# first equilibrium
# -----
ids = pyual.Client(52702, 0, machine='west', user='g2cboulb')

print("Equi")
eqs_in = ids.get('equilibrium', 0, 0)
eqs_in.time = np.array([0])
print("pf_passive")
pf_passive_in = ids.get('pf_passive')
print("wall")
wall_in = ids.get('wall')
print("pf active")
pf_active_in = ids.get('pf_active')
print("iron")
iron_in = ids.get('iron_core')
print("code param")
codeparams = './cedres_param_west_init.xml'
codeparams2 = './cedres_param_west_init2.xml'
eq_in0 = eqs_in
pf_act_in0 = pf_active_in

print('=== CEDRES DIRECT ===')
pf_act0, eq0 = actors.cedres(eq_in0, pf_act_in0, pf_passive_in, wall_in, iron_in, codeparams=codeparams, dbgmode=True)

eq_in1 = ids.get('equilibrium', 0, 35.662)
eq0.time[:] = eq_in1.time[:]

print('=== CEDRES INVERSE ===')
pf_act1, eq1 = actors.cedresinverse(eq0, eq_in1, pf_act_in0, pf_passive_in, wall_in, iron_in,
                                    codeparams=codeparams2, dbgmode=True)

eq_in = deepcopy(eq1)
pf_act_in = deepcopy(pf_act1)

# MQ controller
# =============

# initialize
cfg_file = 'cedres_imas_mqcontrol_cfg.json'
cfg = json.load(open(cfg_file, 'r'))

if cfg['interface']['type'].lower() == 'redis':
    # initialize the Simulink controller
    mq_cont = RedisController(host=cfg['interface']['host'], port=cfg['interface']['port'])

# TIME LOOP
# ---------
while True:
    # send the state to the controller
    to_simulink = pycedres.cpos2simulink(eq_in, pf_act_in)
    mq_cont.put(time=eq_in.time[0], **to_simulink)

    # get inputs from the controller
    sim_out = mq_cont.get()
    print("from Simulink:\n%s" % sim_out)

    if 'terminate' in sim_out['status'].lower():
        # Simulink stops the simulation
        break

    sim_time = sim_out['sim_time']
    print('Simulink time: {}'.format(sim_time))

    cpos = {
        'equilibrium': eq_in,
        'pf_active': pf_act_in,
    }
    cpos = pycedres.simulink2cpos(sim_time, sim_out['in_struct'], cpos)
    eq_in = cpos['equilibrium']
    pf_act_in = cpos['pf_active']
    # set the time
    dt = sim_out['sim_params']['FixedStep']

    # this sets the output time
    # however it would make more sense to set the output time to eq_in.time + dt
    eq_in.time[0] += dt

    # HACK - keep the current constant !!!
    eq_in.time_slice[0].global_quantities.ip = eq0.time_slice[0].global_quantities.ip

    pf_act_out, eq_out = actors.cedresevo(eq_in, pf_act_in, pf_passive_in, wall_in, iron_in, dt,
                                          codeparams=codeparams, dbgmode=True)

    # to_simulink = pycedres.cpos2simulink(eq_out, pf_act_out)
    # # send to the controller
    # mq_cont.put(time=sim_time, **to_simulink)
