function [sim_ports] = sim_cpocontrol_mq_ports(cfg)

n_out_num = 0;
sim_ports = struct();

% inputs for the plant model
sim_ports.in = cfg.from_simulink;

sim_ports.out = struct();

for name = fieldnames(cfg.to_simulink).'
    % outputs from the plant model
    name = name{1};
    prm = cfg.to_simulink.(name);
    n_out_num = n_out_num + 1;
%     sim_ports.out = setfield(sim_ports.out, name, struct());
    sim_ports.out.(name).fun = @mq_port_out;
    sim_ports.out.(name).dim = prm.dim;
    if strcmpi(prm.type, 'double')
        sim_ports.out.(name).Complexity = 'Real';
        sim_ports.out.(name).DatatypeID = 0; % double
    else
        error(['Unknow port type ', prm.type])
    end
    
    %     if iscellstr(prm)
    %         % numbered port
    %         error('not implemented (yet)')
    %         n_out_num = n_out_num + 1;
    %         port_name = ['out', num2str(n_out_num)];
    %         % calculate the dimension
    %         nbc = 1;
    %         for k = 1:length(prm)
    %             outc = prm{k};
    %             if any(outc == '@')
    %                 [tag,sub] = strtok(outc,'@');
    %                 indc      = eval(sub(2:end));
    %                 nbc = nbc + length(indc);
    %             else
    %                 nbc = nbc + 1;
    %             end
    %         end
    %         outPorts.(port_name).dim = nbc - 1;
    %         outPorts.(port_name).fun = prm;
    %         outPorts.(port_name).DatatypeID = 0; % double
    %         % TODO complex output
    %         outPorts.(port_name).Complexity = 'Real';
end

end
