% Level-2 Simulink S-function for cpocontrol plant model

function sim_cpocontrol_mq(block)

setup(block)
% end level-2 S-function
end


function setup(block)

global MQControllerVars

% Dialog parameters (specified in the Simulink GUI)
% 1 - cell array of output port definitions
%     * each cell corresponds to a single output port
%     * cell can either be a cell array contining a list of output
%       values (same as list_output in simmetis)
%     * or it can be a function name
block.NumDialogPrms = 1;
% sizes.NumContStates  = 0;
block.NumContStates = 0;
% sizes.NumDiscStates  = 0;
block.NumDworkDiscStates = 0;

json_text = fileread(block.DialogPrm(1).Data);
MQControllerVars.cfg = loadjson(json_text);

MQControllerVars.sim_params.SolverType = get_param(bdroot, 'SolverType');
if strcmp(MQControllerVars.sim_params.SolverType, 'Fixed-step')
    MQControllerVars.sim_params.FixedStep = get_param(bdroot, 'FixedStep');
else
    MQControllerVars.sim_params.FixedStep = nan;
end
if ~isnumeric(MQControllerVars.sim_params.FixedStep)
    MQControllerVars.sim_params.FixedStep = evalin('base', MQControllerVars.sim_params.FixedStep);
end

sim_ports = sim_cpocontrol_mq_ports(MQControllerVars.cfg);
%     outPorts = struct();
%     port_name = 'outp';
%     dim = 2;
%     fun = @rand;
%     DatatypeID = 0;
%     Complexity = 'Real';
%             outPorts.(port_name).dim = dim;
%         outPorts.(port_name).fun = fun;
%         outPorts.(port_name).DatatypeID = DatatypeID;
%         outPorts.(port_name).Complexity = Complexity;

inputSignals = sim_ports.in;
outPorts = sim_ports.out;
outputSignals = fieldnames(outPorts);


MQControllerVars.PortInfo = struct();
%
%     MQControllerVars.PortInfo.in.cons.input_order = ...
%         {'iso','xece','ip','nbar','picrh','plh','pnbi','pecrh','hmore', 'zeff','ftnbi','flux', ...
%          'R','b0'};

% Register the number of input/output ports
block.NumInputPorts  = length(inputSignals);
block.NumOutputPorts = length(outputSignals);
% Setup port properties to be inherited or dynamic
block.SetPreCompInpPortInfoToDynamic;
block.SetPreCompOutPortInfoToDynamic;

for j=1:length(inputSignals)
    MQControllerVars.PortInfo.in.(inputSignals{j}).num=j;
    % Override input port properties
    % sizes.DirFeedthrough = 1;
    block.InputPort(j).DatatypeID  = 0;  % double
    block.InputPort(j).Complexity  = 'Real';
    block.InputPort(j).DirectFeedthrough = false;
end
for j=1:length(outputSignals)
    MQControllerVars.PortInfo.out.(outputSignals{j}).num=j;
    MQControllerVars.PortInfo.out.(outputSignals{j}).fun=outPorts.(outputSignals{j}).fun;
    block.OutputPort(j).Dimensions  = outPorts.(outputSignals{j}).dim;
    block.OutputPort(j).SamplingMode  = 0;
    block.OutputPort(j).DatatypeID  = outPorts.(outputSignals{j}).DatatypeID;  % double
    block.OutputPort(j).Complexity  = outPorts.(outputSignals{j}).Complexity;
end

% Register sample times
%  [0 offset]            : Continuous sample time
%  [positive_num offset] : Discrete sample time
%
%  [-1, 0]               : Port-based sample time
%  [-2, 0]               : Variable sample time
% sizes.NumSampleTimes = 1;
block.SampleTimes = [-1 0];

%% Set the block simStateCompliance to default (i.e., same as a built-in block)
block.SimStateCompliance = 'DefaultSimState';

%  block.RegBlockMethod('SetOutputPortDimensions', @SetOutPortDims);
block.RegBlockMethod('SetInputPortDimensions',   @SetInPortDims);
block.RegBlockMethod('Outputs',                  @Output);
block.RegBlockMethod('Start',                    @Start);
block.RegBlockMethod('Terminate',                @Terminate);
block.RegBlockMethod('ProcessParameters',        @ProcessPrms);
block.RegBlockMethod('CheckParameters',          @CheckPrms);
block.RegBlockMethod('PostPropagationSetup',     @DoPostPropSetup);
% this is necessary
block.RegBlockMethod('SetInputPortSamplingMode', @SetInpPortFrameData);
end


function SetInpPortFrameData(block, idx, fd)
block.InputPort(idx).SamplingMode = fd;
end


function SetInPortDims(block,port,dims)
block.InputPort(port).Dimensions = dims;
end


function CheckPrms(block)
%
end


function ProcessPrms(block)
% DialogPrm -> RuntimePrm
% block.AutoUpdateRuntimePrms;
end


function DoPostPropSetup(block)
% DialogPrm -> RuntimePrm
% block.AutoRegRuntimePrms;
end


function Start(block)
% Initialize Dwork
% block.Dwork(1).Data = true;
% flag de premier calcul pour cronos
global MQControllerVars
MQControllerVars.out = [];
MQControllerVars.out.index = 0;
end


function Output(block)

global MQControllerVars

VOID = 0; % void definition (used in output functions)

t = block.CurrentTime;


% step 1 (initialization)
if MQControllerVars.out.index == 0
    fprintf('--== First call ==--\n');
    
    %         import org.zeromq.ZMQ
    
    % TODO configure
    if strcmpi(MQControllerVars.cfg.interface.type, 'zmq')
        
        MQControllerVars.zmq_connection.port = '5556';
        MQControllerVars.zmq_connection.open = false;
        MQControllerVars.zmq_connection.context = org.zeromq.ZMQ.context(1);
        MQControllerVars.zmq_connection.socket = MQControllerVars.zmq_connection.context.socket(org.zeromq.ZMQ.PAIR);
        %         MQControllerVars.zmq_connection.socket.setSendTimeOut(1000);
        MQControllerVars.zmq_connection.socket.setReceiveTimeOut(1000);
        
        MQControllerVars.zmq_connection.socket.connect(['tcp://localhost:' MQControllerVars.zmq_connection.port]);
        MQControllerVars.zmq_connection.open = true;
        
    elseif strcmpi(MQControllerVars.cfg.interface.type, 'redis')
        % default redis localhost:6379
        % TODO this could be parametrized
        MQControllerVars.redis_connection.r = redis();
        % one hour timeout
        MQControllerVars.redis_connection.timeout = 3600;
        % ping interval
        MQControllerVars.redis_connection.ping_time = 0.1;
        % generate a random simulation id
        MQControllerVars.redis_connection.id = char(randi([int32('a'), int32('z')], 1, 5));
        fprintf(['REDIS ID: ', MQControllerVars.redis_connection.id, '\n']);
        MQControllerVars.redis_connection.r.rpush('cpocontrol.redis.id', MQControllerVars.redis_connection.id);
        fprintf('The id can be retrieved from cpocontrol.redis.id\n');
    end
    
    % use as the first output
    MQControllerVars.out = [];
    MQControllerVars.out.index = 1;
    
else
    MQControllerVars.out.index = MQControllerVars.out.index + 1;
    fprintf(['--== call ',num2str(MQControllerVars.out.index),' ==--\n']);
    
end

% these are inputs for the tokamak (plant) model
in_struct = struct();
for in_port_name = fieldnames(MQControllerVars.PortInfo.in).'
    in_data = block.InputPort(MQControllerVars.PortInfo.in.(in_port_name{1}).num).Data;
    in_data = reshape(in_data, 1, []);
    in_struct.(in_port_name{1}) = real(in_data);
    if ~isreal(in_data)
        in_struct.([in_port_name{1} '_im']) = imag(in_data);
    end
end
disp(in_struct)

% pfcur = block.InputPort(MQControllerVars.PortInfo.in.pfcur.num).Data;
% pfcur = reshape(pfcur, 1, []);
% fprintf(['Re(pfcur) =~ ', mat2str(real(round(pfcur))), '\n']);
% fprintf(['Im(pfcur) =~ ', mat2str(imag(round(pfcur))), '\n']);


%% call

%     just to test
%     out_struct = struct();
%     out_struct.sim_time = t;
%     out_struct.z = 0;
%     out_struct.ip = norm(pfcur);

% TODO
% - better serialize imaginary numbers
% - automatically browse MQControllerVars.PortInfo.in
zmq_struct_send = struct();
zmq_struct_send.sim_time = t;
zmq_struct_send.status = 'output';
zmq_struct_send.in_struct = in_struct;
zmq_struct_send.sim_params = MQControllerVars.sim_params;
mq_msg = savejson('sim_struct', zmq_struct_send);
disp(mq_msg)
% this is a non-blocking call
if strcmpi(MQControllerVars.cfg.interface.type, 'redis')
    redis_lpush(mq_msg)
    % MQControllerVars.redis_connection.r.lpush('from.simulink', mq_msg)
elseif strcmpi(MQControllerVars.cfg.interface.type, 'zmq')
    MQControllerVars.zmq_connection.socket.send(mq_msg);
else
    error('no connection')
end
disp('sent')

if strcmpi(MQControllerVars.cfg.interface.type, 'redis')
    fprintf('waiting for response  ')
    indicators = 'oO';
    i = 0;
    while true
        % non-blocking call
        i = i + 1;
        zmq_resp = redis_rpop();
        % zmq_resp = MQControllerVars.redis_connection.r.rpop('to.simulink');
        if isempty(zmq_resp)
            indicator = indicators(mod(i, 2) + 1);
            pause(MQControllerVars.redis_connection.ping_time)
            fprintf(['\b' indicator])
        else
            fprintf('\b successful\n')
            break
        end
    end
elseif strcmpi(MQControllerVars.cfg.interface.type, 'zmq')
    while true
        % blocking call
        zmq_resp = MQControllerVars.zmq_connection.socket.recv();
        if isempty(zmq_resp)
            disp('waiting for response')
            pause(5)
        else
            break
        end
    end
else
    error('no connection')
end
disp('received')
if isempty(zmq_resp)
    error('no response')
end
mq_struct_rcv = loadjson(char(zmq_resp(:).'))
if isfield(mq_struct_rcv, 'exception')
    % MQControllerVars.zmq_connection.socket.close()
    % MQControllerVars.zmq_connection.context.close()
    error(['exception received data: ' mq_struct_rcv.exception])
end
% out_struct contains tokamak model outputs
out_struct = mq_struct_rcv.out_struct

% save outputs
MQControllerVars.out.out_struct = out_struct;

% simulink outputs
fnames = fieldnames(MQControllerVars.PortInfo.out);
% loop over output port names
for j=1:length(fnames)
    fun = MQControllerVars.PortInfo.out.(fnames{j}).fun;
    if isa(fun, 'function_handle')
        data = fun(out_struct, fnames{j}, j);
        if isnumeric(data)
            % seems that we need to transpose everything
            data = data.';
        end
        block.OutputPort(MQControllerVars.PortInfo.out.(fnames{j}).num).Data = data;
    else
        keyboard
    end
end
end


function Terminate(block)
%
global MQControllerVars

t = block.CurrentTime;

zmq_struct_send = struct();
zmq_struct_send.sim_time = t;
zmq_struct_send.status = 'terminate';
zmq_struct_send.sim_params = MQControllerVars.sim_params;
mq_msg = savejson('sim_struct', zmq_struct_send);
disp(mq_msg)
% this is a non-blocking call
if strcmpi(MQControllerVars.cfg.interface.type, 'redis')
    redis_lpush(mq_msg)
    % MQControllerVars.redis_connection.r.lpush('from.simulink', mq_msg)
elseif strcmpi(MQControllerVars.cfg.interface.type, 'zmq')
    MQControllerVars.zmq_connection.socket.send(mq_msg);
else
    error('no connection')
end

if strcmpi(MQControllerVars.cfg.interface.type, 'zmq')
    % do we have to do this? - it can block java
    MQControllerVars.zmq_connection.socket.close();
    MQControllerVars.zmq_connection.context.close();
    MQControllerVars.zmq_connection.open = false;
end

end


% REDIS helpers
function zmq_resp = redis_rpop()

global MQControllerVars

zmq_resp = MQControllerVars.redis_connection.r.rpop([MQControllerVars.redis_connection.id, ':to.simulink']);

end

function redis_lpush(mq_msg)

global MQControllerVars

MQControllerVars.redis_connection.r.lpush([MQControllerVars.redis_connection.id, ':from.simulink'], mq_msg);

end